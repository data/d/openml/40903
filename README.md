# OpenML dataset: smtp

https://www.openml.org/d/40903

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The data set is picked from KDD Cup 1999 data, which is available at UCI repository. Using the &lsquo;service&rsquo; attribute, the second largest subsets is Smtp (95,156 records) with anomaly ratios of 0.03%. This dataset is not the original dataset. This dataset is not the original dataset. The target variable is renamed to &quot;Target&quot; and is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40903) of an [OpenML dataset](https://www.openml.org/d/40903). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40903/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40903/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40903/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

